import axios from 'axios'
import { GOOGLE_SPREADSHEET_ID } from '../config.json'

export default async function (orderData) {
  try {
    let res = await axios.post(`https://sheetdb.io/api/v1/${GOOGLE_SPREADSHEET_ID}`, {
      data: [{
        ...orderData,
        id: 'INCREMENT'
      }]
    })
    return res
  } catch (error) {
    console.log(error)
  }
}
