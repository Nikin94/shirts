import axios from 'axios'
import { CLOUDINARY_API_KEY, CLOUDINARY_CLOUD_NAME, CLOUDINARY_API_SECRET } from '../config.json'
import sha1 from 'sha1'
import qs from 'qs'

export default async function (imageData) {
  const timestampUnix = Math.round(+new Date() / 1000)
  const params = {
    api_key: CLOUDINARY_API_KEY,
    timestamp: timestampUnix,
    file: `data:${imageData.mime};base64,${imageData.data}`
  }
  const signature = generateSignature({...params})
  try {
    let res = await axios.post(`https://api.cloudinary.com/v1_1/${CLOUDINARY_CLOUD_NAME}/image/upload`, {
      ...params,
      signature
    })
    return res
  } catch (error) {
    console.log(error)
  }
}

const generateSignature = (body = {}) => {
  const excludedKeys = ['api_key', 'resource_type', 'cloud_name', 'file']
  const sortedObj = Object.keys(body)
    .filter(k => !excludedKeys.includes(k))
    .sort()
    .reduce((obj, key) => ({ ...obj, [key]: body[key] }), {})
  const queryString = qs.stringify(sortedObj) + CLOUDINARY_API_SECRET
  const hash = sha1(queryString)
  return hash
}