import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import BottomSheet from 'reanimated-bottom-sheet';
import ImagePicker from 'react-native-image-crop-picker';

const IMAGE_PROPERTIES = {
  // width: 300,
  // height: 400,
  // cropping: true
  includeBase64: true,
  mediaType: 'photo'
}

export default function Bottom({ selectImage, bottomSheetRef }) {
  const bottomSheetItems = [
    {
      label: 'Take photo...',
      fn: () => takePhotoFromCamera()
    },
    {
      label: 'Choose from library...',
      fn: () => selectPhotoFromGallery()
    }
  ]

  const takePhotoFromCamera = () => {
    ImagePicker.openCamera(IMAGE_PROPERTIES)
      .then(selectImage)
      .catch(err => console.log(err))
  }

  const selectPhotoFromGallery = () => {
    ImagePicker.openPicker(IMAGE_PROPERTIES)
      .then(selectImage)
      .catch(err => console.log(err))
  }

  const renderHeader = () => {
    return pug`
      View(style=styles.rootSheetHeader)
        View(style=styles.bottomSheetHeader)
          View(style=styles.handle)
    `
  }

  const renderContent = () => {
    return pug`
      View(style=styles.rootSheet)
        View(style=styles.bottomSheet)
          Text(style=styles.select) Select avatar
          each item in bottomSheetItems
            TouchableOpacity(
              key=item.label
              style=styles.link
              onPress=item.fn
            )
              Text(style=styles.linkText)= item.label
        View(style=styles.cancel)
          TouchableOpacity(style=styles.cancelButton onPress=() => bottomSheetRef.current.snapTo(2))
            Text(style=styles.cancelText) Cancel
    `
  }

  return pug`
    BottomSheet(
      ref=bottomSheetRef
      snapPoints=[250, 0, 0]
      initialSnap=2
      borderRadius=10
      renderHeader=renderHeader
      renderContent=renderContent
      enabledGestureInteraction=false
      enabledContentTapInteraction=false
    )
  `
}

const styles = StyleSheet.create({
  rootSheetHeader: {
    paddingHorizontal: 10
  },
  bottomSheetHeader: {
    borderColor: '#20232a',
    backgroundColor: '#ececec',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  handle: {
    width: 40,
    height: 6,
    borderRadius: 5,
    backgroundColor: '#ccc'
  },
  rootSheet: {
    paddingHorizontal: 10,
  },
  bottomSheet: {
    paddingVertical: 10,
    backgroundColor: '#ececec',
    flexGrow: 1,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  select: {
    textAlign: 'center',
    marginBottom: 10
  },
  link: {
    alignItems: 'center',
    padding: 10
  },
  linkText: {
    fontSize: 18,
    color: '#177aec'
  },
  cancel: {
    flexGrow: 1,
    marginTop: 10,
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: '#fff'
  },
  cancelButton: {
    borderRadius: 10,
    padding: 10,
    width: '100%',
    alignItems: 'center'
  },
  cancelText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#177aec'
  }
});