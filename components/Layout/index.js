import React, { useRef, useState } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import Bottom from '../Bottom'
import OrderForm from '../OrderForm'

export default function Layout() {
  const [image, setImage] = useState()
  const bottomSheetRef = useRef(null)

  const selectImage = image => {
    if (!image) return
    bottomSheetRef.current.snapTo(2)
    setImage(image)
  }

  return pug`
    ScrollView(contentContainerStyle=styles.root)
      if image
        OrderForm(image=image clear=setImage)
      else
        TouchableOpacity(
          style=styles.selectButton
          onPress=() => bottomSheetRef.current.snapTo(0)
        )
          Text(style=styles.title) Select photo...
    Bottom(selectImage=selectImage bottomSheetRef=bottomSheetRef)
  `
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#aeebff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10
  },
  selectButton: {
    backgroundColor: '#fff',
    borderColor: '#20232a',
    width: 200,
    paddingVertical: 8,
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: 'center'
  },
  title: {
    color: '#20232a'
  }
});