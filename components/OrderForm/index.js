import React, { useRef, useState, useLayoutEffect } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  TextInput
} from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faTshirt } from '@fortawesome/free-solid-svg-icons'
import validator from 'email-validator'
import { uploadImageToCloud, addOrderToSpreadsheet } from '../../helpers'
import Spinner from 'react-native-loading-spinner-overlay'
import Success from './Success'

const CONTAINER_SIZE = {
  width: 150,
  height: 175
}

export default function OrderForm({ image = {}, clear }) {
  const [name, setName] = useState('')
  const [nameError, setNameError] = useState('')

  const [email, setEmail] = useState('')
  const [emailError, setEmailError] = useState('')

  const [loading, setLoading] = useState()
  const [success, setSuccess] = useState()

  const validateData = () => {
    const isEmailValid = validator.validate(email)
    const isError = !name || !email || !isEmailValid
    if (isError) {
      if (!name) setNameError('Name must not be empty')
      if (!isEmailValid) setEmailError('Enter valid email')
      if (!email) setEmailError('Email must not be empty')
      return false
    }
    return true
  }

  const submit = async () => {
    if (!validateData()) return
    setLoading(true)
    const uploadRes = await uploadImageToCloud(image)
    if (uploadRes.status !== 200) return setLoading()
    const data = {
      name,
      email,
      imageUrl: uploadRes.data.url,
      date: uploadRes.data.created_at
    }
    const addOrderRes = await addOrderToSpreadsheet(data)
    if (addOrderRes.status === 201 && addOrderRes.data.created) setSuccess(true)
    setLoading()
  }

  if (success) {
    return pug`
      Success(onPress=clear)
    `
  }

  return pug`
    View(style=styles.root)
      Spinner(visible=loading textContent='Loading...')
      View(style=styles.wrapper)
        FontAwesomeIcon(icon=faTshirt style=styles.icon size=350)
        View(style=styles.container)
          Image(
            source={uri: image.path}
            style={
              width: CONTAINER_SIZE.width,
              height: CONTAINER_SIZE.height
            }
            resizeMode='contain'
          )
      View(style=styles.errorWrapper)
        TextInput(
          style=styles.input
          placeholder='Your name'
          value=name
          onBlur=() => setName(name.replace(/ {1,}/g," ").trim())
          onChangeText=text => {
            setNameError('')
            setName(text)
          }
        )
        if nameError
          Text(style=styles.error)= nameError
      View(style=styles.errorWrapper)
        TextInput(
          style=styles.input
          placeholder='Your email'
          value=email
          onChangeText=text => {
            setEmailError('')
            setEmail(text.trim())
          }
        )
        if emailError
          Text(style=styles.error)= emailError
      View(style=styles.row)
        TouchableOpacity(style=styles.submit onPress=() => submit())
          Text(style=styles.submitText) Order now
        TouchableOpacity(style=styles.retry onPress=() => clear())
          Text(style=styles.cancelText) Retry
  `
}

const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%'
  },
  wrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderStyle: 'dashed',
    borderRadius: 10,
    borderColor: '#dadce0',
    position: 'absolute',
    width: CONTAINER_SIZE.width,
    height: CONTAINER_SIZE.height,
    top: 110,
    left: '50%',
    marginLeft: - CONTAINER_SIZE.width / 2,
    right: 0,
    bottom: 0,
    overflow: 'hidden'
  },
  icon: {
    color: '#fff'
  },
  errorWrapper: {
    width: '100%',
    marginTop: 8
  },
  input: {
    backgroundColor: '#fff',
    width: '100%',
    paddingHorizontal: 16
  },
  error: {
    color: '#f00',
    marginTop: 2
  },
  row: {
    marginTop: 8,
    flexDirection: 'row'
  },
  submit: {
    padding: 8,
    flexGrow: 1,
    backgroundColor: '#518df4',
    alignItems: 'center',
    justifyContent: 'center'
  },
  submitText: {
    fontWeight: 'bold',
    textTransform: 'uppercase',
    color: '#fff'
  },
  retry: {
    width: 75,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 8,
    backgroundColor: '#f44336'
  },
  cancelText: {
    color: '#fff'
  }
});