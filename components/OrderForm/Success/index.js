import React, { useRef, useState } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity
} from 'react-native';

export default function Success({ onPress }) {
  return pug`
    View(style=styles.root)
      View(style=styles.success)
        Text(style=styles.successText) Success!
      TouchableOpacity(style=styles.back onPress=() => onPress())
        Text(style=styles.backText) Back to main
  `
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  success: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  successText: {
    textTransform: 'uppercase',
    color: '#fff',
    fontSize: 64,
    fontWeight: 'bold',
  },
  back: {
    backgroundColor: '#fff',
    marginBottom: 24,
    padding: 8,
    borderRadius: 4
  }
});